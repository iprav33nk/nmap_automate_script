

host="localhost"
project_path="/home/praveenk/Documents/we45/test_project/"


echo "-------------------"
echo "Scanning the Host  :" $host
echo "-------------------"

echo "-------------------"
echo "Project Path  :" $project_path
echo "-------------------"


function tcp_scan(){
	echo "tcp_scan"
	nmap -sT -T1 $host -p 1-65 -Pn -oA $project_path/TCP_list1
}

function udp_scan(){
	echo "udp_scan"
	sudo nmap -sU $host -Pn -oA $project_path/UDP_list1
}

function discovery(){
	echo "discovery"
	nmap -sV -sC --script=discovery $host -Pn -oA $project_path/disc
}

function vuln_scan(){
	echo "vuln_scan"
	nmap -sV -sC --script=vuln $host -Pn -oA $project_path/vuln_op
}

function auth_scan(){
	echo "auth_scan"
	nmap -sV -sC --script=http-auth.nse $host -Pn -oA $project_path/auth_op
}

function default_scan(){
	echo "default_scan"
	nmap -sV -sC --script=default $host -Pn -oA $project_path/default_op
}

function fuzzer_scan(){
	echo "fuzzer_scan"
	nmap -sV -sC --script=fuzzer $host -Pn -oA $project_path/fuzzer_op
}

function exploit_scan(){
	echo "exploit_scan"
	nmap -sV -sC --script=exploit $host -Pn -oA $project_path/exploit_op
}

function heartbleed_scan(){
	echo "heartbleed_scan"
	nmap -sV --script=ssl-heartbleed $host -oA $project_path/HeartBleed
}

function poodle_scan(){
	echo "poodle_scan"
	nmap -sV --script ssl-poodle $host -oA $project_path/Poodle.xml
}


tcp_scan
udp_scan
discovery
vuln_scan
auth_scan
default_scan
fuzzer_scan
exploit_scan
heartbleed_scan
poodle_scan